# Accelerate convolutional neural networks using F1 istances 

This repository is to help user to run precompiled AFI containing accelerated CNN using dataflow computational pattern on Amazon F1 instances.

**Demo CNN**:

- Lenet
- Usps


#### Start a EC2 F1 instance based FPGA developer AMI
1. Sign into your AWS account and go to EC2
2. Select Region US-West (Ireland).
3. Launch an F1 Instance (f1.2xlarge) using the FPGA developer AMI from the EC2 Console.
4. Connect to your instance using SSH

```bash
ssh -i <.pem file> centos@<IPv4 Public IP>
```

For more details please refer to: 

[https://github.com/Xilinx/SDAccel_Examples/wiki/Create,-configure-and-test-an-AWS-F1-instance]()

#### Initialize environment

```bash
cd ~
git clone https://bitbucket.org/necst/condor-aws-raw2018.git
git clone https://github.com/aws/aws-fpga.git

cd aws-fpga
source sdaccel_setup.sh && source sdk_setup.sh
```

#### Load AFI

Check the table below to find the agfi of the desired convolutional neural network:

|  CNN | AGFI |  
|---|---|
|  USPS |  agfi-01e47f8d53bc08c5a | 
|  LeNet | agfi-00cc8533462eab688 |


```bash
sudo fpga-clear-local-image -S 0
sudo fpga-load-local-image -S 0 -I <desired agfi>
sudo fpga-describe-local-image -S 0 -H
```

#### Compile source code 

Move to the desired folder of the demo and compile sources using make. 

 
```bash
cd ~/condor-aws-raw2018/usps/
make 
```

or 

```bash
cd ~/condor-aws-raw2018/lenet/
make 
```

#### Run

Now you're ready to run the selected example!

```bash
sudo sh
source /opt/Xilinx/SDx/2017.1.rte/setup.sh   
./usps-time
``` 
