#include "xcl2.hpp"

#include <vector>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>

/***************** Image informations *******************/

static const int HEIGHT = 16;
static const int WIDTH = 16;
static const int DEPTH = 1;
static const int IMAGE_SIZE = HEIGHT * WIDTH * DEPTH;
static const int PORT_WIDTH = 8;

/***************** Timing Informations *******************/

// Number of Images for the Test
static const int N_IMAGES = 50; 

// Number of tests to average for final result
static const int N_TESTS = 5; 

// More informations
static const bool debug = false;

using std::vector;
typedef float data_type;

/**********************  Test Function  ***************************/

void launch_kernel(int images, cl::CommandQueue& q, cl::Kernel& kernel){
    
    if (debug)
        std::cout << "Launch the kernel with " << images << " images" << std::endl;

    // kernel_usps.setArg(0, buffer_input);
    // kernel_usps.setArg(1, buffer_output);
    kernel.setArg(2, images * IMAGE_SIZE);
    kernel.setArg(3, images * 10);

    uint64_t total_test_time = 0;

    for (int i = 0; i < N_TESTS; ++i)
    {
        cl::Event event;
        uint64_t nstimestart, nstimeend;
        
        q.enqueueTask(kernel, NULL, &event);
        q.finish();

        event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_START,&nstimestart);
        event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_END,&nstimeend);
        uint64_t simple_time = nstimeend - nstimestart;
        total_test_time += simple_time;
	if (debug)
            printf("| %-9s %d | %-20lu |\n", "Test", i, simple_time);
    }

    uint64_t avg_test_time = total_test_time / N_TESTS;
    printf("| %-10d  | %-20lu |\n", images, avg_test_time / images);
    return;

}

int main(int argc, char **argv) {

    int input_size = IMAGE_SIZE * N_IMAGES;
    int output_size = N_IMAGES * 10;

    int in_buffer_size = ((input_size - 1) / PORT_WIDTH + 1) * PORT_WIDTH;
    int out_buffer_size = ((output_size - 1) / PORT_WIDTH + 1) * PORT_WIDTH;

    size_t in_buffer_size_bytes = in_buffer_size * sizeof(data_type);
    size_t out_buffer_size_bytes = out_buffer_size * sizeof(data_type);

    vector<float,aligned_allocator<float>> v_input(in_buffer_size, 1);
    vector<float,aligned_allocator<float>> v_output(out_buffer_size, 0);

    std::vector<cl::Device> devices = xcl::get_xil_devices();
    cl::Device device = devices[0];

    cl::Context context(device);
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE);
    std::string device_name = device.getInfo<CL_DEVICE_NAME>();
    std::cout << "Found Device=" << device_name.c_str() << std::endl;

    // Import binary fle
    std::string binaryFile = xcl::find_binary_file(device_name, "usps");
    cl::Program::Binaries bins = xcl::import_binary_file(binaryFile);
    devices.resize(1);
    cl::Program program(context, devices, bins);    

    // Allocate memory on the FPGA
    cl::Buffer buffer_input(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,
                        in_buffer_size_bytes, v_input.data());
    cl::Buffer buffer_output(context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,
                        out_buffer_size_bytes, v_output.data());

    std::vector<cl::Memory> inBufVec, outBufVec;
    inBufVec.push_back(buffer_input);
    outBufVec.push_back(buffer_output);

    q.enqueueMigrateMemObjects(inBufVec,0);

    cl::Kernel kernel_usps(program,"network");

    std::vector<cl::Event> events(N_IMAGES*N_TESTS);
    kernel_usps.setArg(0, buffer_input);
    kernel_usps.setArg(1, buffer_output);
    printf("| %-10s  | %-20s |\n", "Images", "Time per Image");
    for (int images = 1; images <= N_IMAGES; ++images){
        launch_kernel(images, q, kernel_usps);
    }

    std::cout << "Done" << std::endl;
    
    return 0;
    }
